use std::env;
use std::fs;
use std::io;

fn main() {
    let content = get_content().unwrap();
    let mut stack = make_stack(content);
    run(&mut stack);
}

fn get_content() -> Result<String, Box<dyn std::error::Error + 'static>> {
    let args: Vec<String> = env::args().collect();
    let content = fs::read_to_string(&args[1])?;
    Ok(content)
}

fn make_stack(s : String) -> Vec<isize> {
    s
        .trim()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect()
}

#[derive (Copy, Clone)]
enum Val {
    Direct(usize),
    Indirect(usize)
}

impl Val {
    fn get(&self, stack : &mut Vec<isize>) -> isize {
        match self {
            Val::Direct(i) => {
                return stack[*i];
            },
            Val::Indirect(i) => {
                let u : usize = stack[*i].abs().to_string().parse().unwrap();
                return stack[u];
            }
        }
    }

    fn set(&self, val : isize, stack : &mut Vec<isize>) {
        match self {
            Val::Direct(i) => {
                stack[*i] = val;
            },
            Val::Indirect(i) => {
                let u : usize = stack[*i].abs().to_string().parse().unwrap();
                stack[u] = val;
            }
        }
    }

    fn new(mode : isize, pointer : usize) -> Val {
        match mode {
            0 => return Val::Indirect(pointer),
            1 => return Val::Direct(pointer),
            _ => panic!(),
        }
    }
}

enum Inst {
    Quit,
    Out(Val),
    In(Val),
    Add(Val, Val, Val),
    Mult(Val, Val, Val),
}

impl Inst {
    fn exec(&self, stack : &mut Vec<isize>) {
        match self {
            Inst::Quit => {
                println!("HALT");
                println!("{}", stack[0]);
                panic!();
            },
            Inst::Out(val) => {
                println!("{}", val.get(stack));
            },
            Inst::In(val) => {
                println!("Input: ");
                let mut input = String::new();
                io::stdin().read_line(&mut input).unwrap();
                let input : isize = input.chars().filter(|x| x.is_digit(10)).collect::<String>().parse().unwrap();

                val.set(input, stack);
            },
            Inst::Add(fst, snd, dest) => {
                dest.set(fst.get(stack) + snd.get(stack), stack);
            },
            Inst::Mult(fst, snd, dest) => {
                dest.set(fst.get(stack) * snd.get(stack), stack);
            }
        }
    }

    fn parse(pointer : &mut usize, stack : &mut Vec<isize>) -> Inst {
        let intcode = stack[*pointer];
        let opcode = intcode % 100;
        let thrdmode = (intcode - opcode) % 1000 / 100;
        let sndmode = (intcode - opcode - thrdmode) % 10000 / 1000;
        let fstmode = (intcode - opcode - thrdmode - sndmode) % 100000 / 10000;
        let modes = vec![fstmode, sndmode, thrdmode];

        let vals = modes.iter().enumerate().map(|(i, mode)| Val::new(*mode, *pointer + i + 1)).collect::<Vec<Val>>();

        match opcode {
            99 => {
                return Inst::Quit;
            },
            1 => {
                *pointer += 4;
                return Inst::Add(vals[0], vals[1], vals[2]);
            }
            2 => {
                *pointer += 4;
                return Inst::Mult(vals[0], vals[1], vals[2]);
            },
            3 => {
                *pointer += 2;
                return Inst::In(vals[0]);
            }
            4 => {
                *pointer += 2;
                return Inst::Out(vals[0]);
            }
            _ => panic!()
        }
    }
}

fn run(mut stack : &mut Vec<isize>) {
    let mut pointer = 0;
    loop {
        Inst::parse(&mut pointer, &mut stack).exec(stack)
    }
}
